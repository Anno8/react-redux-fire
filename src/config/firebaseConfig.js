import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCU1VZ4dVv8E3wD4RGa_D_ibNB5Ww6NKzM",
  authDomain: "summon-agile.firebaseapp.com",
  databaseURL: "https://summon-agile.firebaseio.com",
  projectId: "summon-agile",
  storageBucket: "",
  messagingSenderId: "641883910112",
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
