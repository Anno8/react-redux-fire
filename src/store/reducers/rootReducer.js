import { AuthReducer } from "./authReducer";
import { PostReducer } from "./postReducer";
import { combineReducers } from "redux";
import { firestoreReducer } from "redux-firestore";
import { firebaseReducer } from "react-redux-firebase";

export const RootReducer = combineReducers({
  auth: AuthReducer,
  post: PostReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer
});
