const initState = {
  posts: []
};

// Not really needed since I relay on react redux firebase for the data
export const PostReducer = (state = initState, action) => {
  switch (action.type) {
    case "CREATE_POST":
      return state;
    case "CREATE_POST_ERROR":
      return state;
    default:
      return state;
  }
};
