export const signIn = ({email, password}) => (dispatch, getState, { getFirebase }) => {
  const firebase = getFirebase();

  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(() => dispatch({ type: "LOGIN_SUCCESS" }))
    .catch(err => dispatch({ type: "LOGIN_ERROR", err }));
};

export const signOut = () => (dispatch, getState, { getFirebase }) => {
  const firebase = getFirebase();

  firebase
    .auth()
    .signOut()
    .then(() => dispatch({type: "SIGNOUT_SUCCESS" }))
    .catch(() => {});
}

export const signUp = ({ email, password, firstName, lastName }) => (dispatch, getState, { getFirebase, getFirestore }) => {
  const firebase = getFirebase();
  const firestore = getFirestore();

  firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(({ user }) => 
      firestore
        .collection("users")
        .doc(user.uid)
        .set({
          email,
          firstName: firstName,
          lastName: lastName,
          initials: firstName[0] + lastName[0]
        })
    )
    .then(() => dispatch({type: "SIGNUP_SUCCESS"}))
    .catch(err => dispatch({type: "SIGNUP_ERROR", err}));
}