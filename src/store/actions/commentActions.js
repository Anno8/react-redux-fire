export const createComment = ({comment, postId}) => (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const profile = getState().firebase.profile;
    const authorId = getState().firebase.auth.uid;
    
    firestore.collection("comments").add({
      comment,
      postId,
      authorFirstName: profile.firstName,
      authorLastName: profile.lastName,
      authorId,
      createdAt: new Date()
    })
    .then(() => dispatch({ type: "CREATE_COMMENT", comment }))
    .catch(err => dispatch({type: "CREATE_COMMENT_ERROR", err }));
  };
  