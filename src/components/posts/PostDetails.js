import React from "react";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import { Redirect } from "react-router-dom";
import moment from "moment";
import { CommentList } from "../comments/CommentList";
import CreateComment from "../comments/CreateComment";

const PostDetails = ({ post, comments, auth, postId }) => {
  return(
    auth.uid ? 
      post ? 
        <div className="container section post-details">
          <div className="card z-depth-0">
            <div className="card-content">
              <span className="card-title">{post.title}</span>
              <p> { post.content } </p>
            </div>
            <div className="card-action gret lighten-4 grey-text">
              <div>Posted by {post.authorFirstName} {post.authorLastName}</div>
              <div>{moment(post.createdAt.toDate()).calendar()}</div>
            </div>
            <div className="card-action gret lighten-4 grey-text">
              <CommentList comments={comments}/>
              <CreateComment postId={postId} />
            </div>
          </div>
        </div>
        :
        <div className="container center">
          <p>Loading Post...</p>
        </div> 
      :
      <Redirect to="/signin" />)
  }

const mapStateToProps = (state, ownProps) => ({
  post: state.firestore.data.posts ? state.firestore.data.posts[ownProps.match.params.id] : null,
  // Additional overhead filter to avoid the delay in cleaning the comments on posts that have no relation with the given post
  comments: state.firestore.ordered.comments ? state.firestore.ordered.comments.filter(c => c.postId === ownProps.match.params.id) : [],
  postId: ownProps.match.params.id,
  auth: state.firebase.auth
});

export default compose(
  connect(mapStateToProps),
  firestoreConnect(({ postId }) => ([
    { collection: "posts"},
    { collection: "comments", where: ["postId", "==", postId], orderBy: ["createdAt", "desc"]}
  ]))
)(PostDetails);
