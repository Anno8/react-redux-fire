import React from "react";
import { PostSummary } from "./PostSummary";
import { Link } from "react-router-dom";

export const PostsList = ({ posts }) => (
  <div className="post-list section">
    {posts &&
      posts.map(p => (
        <Link key={p.id} to={`/post/${p.id}`}>
          <PostSummary post={p} />
        </Link>
      ))}
  </div>
);
