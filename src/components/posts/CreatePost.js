import React from "react";
import { createPost } from "../../store/actions/postActions";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { useFormInput } from "../../hooks/hooks";

const CreatePost = (props) => {
  const title = useFormInput("");
  const content = useFormInput("");

  const handleSubmit = e => {
    e.preventDefault();
    props.createPost({title: title.value, content: content.value});
    props.history.push("/");
  };

  return (
    props.auth.uid ? 
      <div className="container">
        <form className="white" onSubmit={handleSubmit}>
          <h5 className="grey-text text-darken-3">Create new post</h5>
          <div className="input-field">
            <label htmlFor="title">Title</label>
            <input type="text" id="title" {...title} pattern=".{6,25}" required title="Title needs to be at least 6 characters and maximum 25 characters long" />
          </div>
          <div className="input-field">
            <label htmlFor="content">Content</label>
            <textarea className="materialize-textarea" type="text" id="content" {...content} />
          </div>
          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0">Create</button>
          </div>
        </form>
      </div>
      :
      <Redirect to="/signin"/>
  );
}

const mapStateToProps = (state) => ({
  auth: state.firebase.auth
});

const mapDispatchToProps = (dispatch) => ({
  createPost: (post) => dispatch(createPost(post))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreatePost);
