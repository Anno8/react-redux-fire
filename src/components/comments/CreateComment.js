import React, { useState } from "react";
import { createComment } from "../../store/actions/commentActions";
import { connect } from "react-redux";

const CreateComment = (props) => {
  const [content, setContent] = useState("");
  const handleSubmit = e => {
    e.preventDefault();
    props.createComment({comment: content, postId: props.postId});
    setContent("");
  };

  return (
      <form className="white" onSubmit={handleSubmit}>
        <div className="input-field">
          <label htmlFor="content">Comment</label>
          <textarea className="materialize-textarea" type="text" id="content" value={content} onChange={e => setContent(e.target.value)} />
        </div>
        <div className="input-field">
          <button className="btn pink lighten-1 z-depth-0">Create</button>
        </div>
      </form>);
}

const mapDispatchToProps = (dispatch) => ({
  createComment: (data) => dispatch(createComment(data))
});

export default connect(null, mapDispatchToProps)(CreateComment);
