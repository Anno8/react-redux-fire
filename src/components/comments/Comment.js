import React from "react";
import moment from "moment";

export const Comment = ({ comment }) => (
  <div className="card z-depth-2 post-summary">
    <div className="card-content grey-text text-darken-3">
      <p>Posted by {comment.authorFirstName} {comment.authorLastName}</p>
      <hr/>
      {comment.comment}
      <p className="grey-text">{moment(comment.createdAt.toDate()).calendar()}</p>
    </div>
  </div>
);
