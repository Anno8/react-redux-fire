import React from "react";
import { Comment } from "./Comment";

export const CommentList = ({ comments }) => (
  <div className="comment-list section">
    <p>Comments</p>
    {comments && comments.map(c => <Comment comment={c} key={c.id} />)}
  </div>
);
