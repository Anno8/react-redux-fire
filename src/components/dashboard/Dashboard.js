import React from "react";
import { Notifications } from "./Notifications";
import { PostsList } from "../posts/PostsList";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";

const Dashboard = ({ posts, notifications }) => {
  return (
    <div className="dashboard container">
      <div className="row">
        <div className="col s12 m6">
          <PostsList posts={posts} />
        </div>
        <div className="col s12 m5 offset-m1">
          <Notifications notifications={notifications} />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  posts: state.firestore.ordered.posts,
  notifications: state.firestore.ordered.notifications,
  auth: state.firebase.auth
});

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: "posts", orderBy: ["createdAt", "desc"] },
    { collection: "notifications", limit: 4, orderBy: ["time", "desc"] }
  ])
)(Dashboard);
