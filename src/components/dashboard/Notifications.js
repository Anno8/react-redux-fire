import React from "react";
import moment from "moment";
import { NavLink } from "react-router-dom";

export const Notifications = ({ notifications }) => (
  <div className="section">
    <div className="card z-depth-1">
      <div className="card-content">
        <span className="card-title">Notifications</span>
        <ul className="notifications">
          {notifications &&
            notifications.map(n => (
              <li key={n.id}>
                <span className="pink-text"> {n.user}</span>
                <span> {n.content} {n.type === "postCreated" ? <NavLink to={`/post/${n.postId}`}>{n.title}</NavLink> : null} </span>
                <span> {moment(n.time.toDate()).fromNow()}</span>
              </li>
            ))}
        </ul>
      </div>
    </div>
  </div>
);
