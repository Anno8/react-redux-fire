import React from "react";
import { Link } from "react-router-dom";
import SignedInLinks from "./SignedInLinks";
import { SignedOutLinks } from "./SignedOutLinks";
import { connect } from "react-redux";

const Navbar = ({ auth, profile }) => (
  <nav className="nav-wrapper grey darken-4">
      <Link to="/" className="brand-logo left">
        Agile
      </Link>
      {auth.uid ? <SignedInLinks profile={profile} /> : <SignedOutLinks />}
  </nav>
);

const mapStateToProps = state => ({
  auth: state.firebase.auth,
  profile: state.firebase.profile
});

export default connect(mapStateToProps)(Navbar);
