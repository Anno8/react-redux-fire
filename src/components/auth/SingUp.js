import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { signUp } from "../../store/actions/authActions";
import { useFormInput } from "../../hooks/hooks";

const SingUp = props => {
  const firstName = useFormInput("");
  const lastName = useFormInput("");
  const email = useFormInput("");
  const password = useFormInput("");

  const handleSubmit = e => {
    e.preventDefault();
    props.signUp({
      firstName: firstName.value,
      lastName: lastName.value,
      email: email.value,
      password: password.value
    });
  };

  return props.auth.uid ? (
    <Redirect to="/" />
  ) : (
    <div className="container">
      <form className="white" onSubmit={handleSubmit}>
        <h5 className="grey-text text-darken-3">Sign Up</h5>
        <div className="input-field">
          <label htmlFor="email">Email</label>
          <input type="email" id="email" {...email} required />
        </div>
        <div className="input-field">
          <label htmlFor="firstName">Name</label>
          <input type="text" id="firstName" {...firstName} required />
        </div>
        <div className="input-field">
          <label htmlFor="lastName">Surname</label>
          <input type="text" id="lastName" {...lastName} required />
        </div>
        <div className="input-field">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            {...password}
            pattern=".{6,}"
            required
            title="Password needs to be at least 6 characters long"
          />
        </div>
        <div className="input-field">
          <button className="btn pink lighten-1 z-depth-0">Register</button>
          <div className="red-text center">
            {props.authError || <p> {props.authError} </p>}
          </div>
        </div>
      </form>
    </div>
  );
};

const mapStateToProps = state => ({
  authError: state.auth.authError,
  auth: state.firebase.auth
});

const mapDispatchToProps = dispatch => ({
  signUp: newUser => dispatch(signUp(newUser))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SingUp);
