import React from "react";
import { connect } from "react-redux";
import { signIn } from "../../store/actions/authActions";
import { Redirect } from "react-router-dom";
import { useFormInput } from "../../hooks/hooks";

const SignIn = props => {
  const email = useFormInput("");
  const password = useFormInput("");

  const handleSubmit = e => {
    e.preventDefault();
    props.signIn({ email: email.value, password: password.value });
  };

  return props.auth.uid ? (
    <Redirect to="/" />
  ) : (
    <div className="container">
      <form className="white" onSubmit={handleSubmit}>
        <h5 className="grey-text text-darken-3">Sign In</h5>
        <div className="input-field">
          <label htmlFor="email">Email</label>
          <input type="email" id="email" {...email} required />
        </div>
        <div className="input-field">
          <label htmlFor="password">Password</label>
          <input type="password" id="password" {...password} required />
        </div>
        <div className="red-text center">
          {props.authError && <p>{props.authError}</p>}
        </div>
        <div className="input-field">
          <button className="btn pink lighten-1 z-depth-0">Login</button>
        </div>
      </form>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  signIn: credentials => dispatch(signIn(credentials))
});

const mapStateToProps = state => ({
  authError: state.auth.authError,
  auth: state.firebase.auth
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);
